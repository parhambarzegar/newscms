<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
 $check = getimagesize($_FILES["image"]["tmp_name"]);
 if ($check == true) {
 $target_dir = "pictures/";
 $target_file = $target_dir . basename($_FILES["image"]["name"]);
 $uploadOk = 1;
 $imageFileType = pathinfo(basename($_FILES["image"]["name"]),PATHINFO_EXTENSION);
 // Check if image file is a actual image or fake image
     $check = getimagesize($_FILES["image"]["tmp_name"]);
     if($check !== false) {
         echo "File is an image - " . $check["mime"] . ".";
         $uploadOk = 1;
     } else {
         echo "File is not an image.";
         $uploadOk = 0;
     }
 // Check if file already exists
 if (file_exists($target_file)) {
     echo "Sorry, file already exists.";
     $uploadOk = 0;
   }
 // Check file size
 if ($_FILES["image"]["size"] > 50000000) {
   echo "Sorry, your file is too large.";
   $uploadOk = 0;
 }
 if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
   echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
   $uploadOk = 0;
 }
 //check if upload ok is 0
 if ($uploadOk == 0) {
   echo "Sorry, your file was not uploaded.";
   //uploads file if uploadok is 1
} else {
   if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
         echo "The file ". basename( $_FILES["image"]["name"]). " has been uploaded.";
         $image = 'pictures/'.$_FILES['image']['name'];
         $sqlinsert = "insert into posts (title, subtitle, text, category, date, visittimes, image) values ('".$_POST['title']."', '".$_POST['subtitle']."', '".$_POST['text']."', '".$_POST['category']."', '".date('Y-m-d')."', '0', '".$image."')";
         if(mysqli_query($conn, $sqlinsert)) {
           echo "post added successfully";
         } else {
           echo "ERROR: ".mysqli_error($conn);
         }
       } else {
           echo "Sorry, there was an error uploading your file.";
         }

     }
   } else {
     echo "<span style='color:red;'><br>no image! you have to select one.</span>";
   }
 }
?>
