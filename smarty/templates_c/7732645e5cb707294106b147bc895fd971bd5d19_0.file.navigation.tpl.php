<?php
/* Smarty version 3.1.30, created on 2017-01-23 12:50:59
  from "/usr/share/nginx/html/smartypr/smarnt/templates/navigation.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5885fc33580cd2_59409881',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7732645e5cb707294106b147bc895fd971bd5d19' => 
    array (
      0 => '/usr/share/nginx/html/smartypr/smarnt/templates/navigation.tpl',
      1 => 1485175858,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5885fc33580cd2_59409881 (Smarty_Internal_Template $_smarty_tpl) {
?>
<link rel="stylesheet" href="css/nav.css">
<link rel="stylesheet" href="css/master.css">
<ul class="menu">
    <li><a href="index.php"><?php echo $_smarty_tpl->tpl_vars['home']->value;?>
</a></li>
    <li><a href="#"><?php echo $_smarty_tpl->tpl_vars['news']->value;?>
</a>
        <ul>
            <li><a style="vertical-align: middle;" href="index.php?category=irannews"> <?php echo $_smarty_tpl->tpl_vars['irannews']->value;?>
</a></li>
            <li><a href="index.php?category=worldnews"> <?php echo $_smarty_tpl->tpl_vars['worldnews']->value;?>
</a></li>
            <li><a href="index.php?category=sports"><?php echo $_smarty_tpl->tpl_vars['sportnews']->value;?>
</a></li>
        </ul>
    </li>
    <li><a href="contact.php"> <?php echo $_smarty_tpl->tpl_vars['contact']->value;?>
</a></a></li>
    <li><a href="cms.php"> <?php echo $_smarty_tpl->tpl_vars['login']->value;?>
</a></li>
    <li><a href="aboutus.php"><?php echo $_smarty_tpl->tpl_vars['about']->value;?>
</a></li>
</ul>
<?php }
}
